//
//  ListCell.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import UIKit

class ListCell: UICollectionViewCell {
    static let nibName = "ListCell"
    
    @IBOutlet private var textLabel: UILabel!
    var text: String? {
        didSet {
            textLabel.text = text
        }
    }
}
