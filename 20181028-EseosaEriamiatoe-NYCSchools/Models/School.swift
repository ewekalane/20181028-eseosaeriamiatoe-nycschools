//
//  School.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import Foundation
import IGListKit


class School: Codable {
    let academicopportunities1, academicopportunities2, admissionspriority11, admissionspriority21: String?
    let admissionspriority31, attendanceRate, bbl, bin: String?
    let boro, borough, buildingCode, bus: String?
    let censusTract, city, code1, communityBoard: String?
    let councilDistrict, dbn, directions1, ellPrograms: String?
    let extracurricularActivities, faxNumber, finalgrades, grade9Geapplicants1: String?
    let grade9Geapplicantsperseat1, grade9Gefilledflag1, grade9Swdapplicants1, grade9Swdapplicantsperseat1: String?
    let grade9Swdfilledflag1, grades2018, interest1, latitude: String?
    let location, longitude, method1, neighborhood: String?
    let nta, offerRate1, overviewParagraph, pctStuEnoughVariety: String?
    let pctStuSafe, phoneNumber, primaryAddressLine1, program1: String?
    let requirement11, requirement21, requirement31, requirement41: String?
    let requirement51, school10ThSeats, schoolAccessibilityDescription, schoolEmail: String?
    let schoolName, schoolSports, seats101, seats9Ge1: String?
    let seats9Swd1, stateCode, subway, totalStudents: String?
    let website, zip, name: String?
    let founded: Int?
    let members: [String]?
    
    enum CodingKeys: String, CodingKey {
        case academicopportunities1, academicopportunities2, admissionspriority11, admissionspriority21, admissionspriority31
        case attendanceRate = "attendance_rate"
        case bbl, bin, boro, borough
        case buildingCode = "building_code"
        case bus
        case censusTract = "census_tract"
        case city, code1
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case dbn, directions1
        case ellPrograms = "ell_programs"
        case extracurricularActivities = "extracurricular_activities"
        case faxNumber = "fax_number"
        case finalgrades
        case grade9Geapplicants1 = "grade9geapplicants1"
        case grade9Geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9Gefilledflag1 = "grade9gefilledflag1"
        case grade9Swdapplicants1 = "grade9swdapplicants1"
        case grade9Swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case grade9Swdfilledflag1 = "grade9swdfilledflag1"
        case grades2018, interest1, latitude, location, longitude, method1, neighborhood, nta
        case offerRate1 = "offer_rate1"
        case overviewParagraph = "overview_paragraph"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case pctStuSafe = "pct_stu_safe"
        case phoneNumber = "phone_number"
        case primaryAddressLine1 = "primary_address_line_1"
        case program1
        case requirement11 = "requirement1_1"
        case requirement21 = "requirement2_1"
        case requirement31 = "requirement3_1"
        case requirement41 = "requirement4_1"
        case requirement51 = "requirement5_1"
        case school10ThSeats = "school_10th_seats"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case schoolEmail = "school_email"
        case schoolName = "school_name"
        case schoolSports = "school_sports"
        case seats101
        case seats9Ge1 = "seats9ge1"
        case seats9Swd1 = "seats9swd1"
        case stateCode = "state_code"
        case subway
        case totalStudents = "total_students"
        case website, zip, name, founded, members
    }
}


class SchoolDetail: Codable {
    let dbn, numOfSatTestTakers, satCriticalReadingAvgScore, satMathAvgScore: String
    let satWritingAvgScore, schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case schoolName = "school_name"
    }
}



extension School: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return dbn! as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object {
            return true
        }
        guard let object = object as? School else {
            return false
        }
        return dbn == object.dbn
    }
    
}


