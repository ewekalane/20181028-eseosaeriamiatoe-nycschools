//
//  NetworkAdapter.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import Foundation
import Moya


struct networkAdapter {
    static let provider = MoyaProvider<NetworkAPI>()
    
    static func request(target: NetworkAPI, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    if response.statusCode >= 200 && response.statusCode <= 300 {
                        successCallback(response)
                    } else {
                        let error = NSError(domain:"com.20181028-EseosaEriamiatoe-NYCSchools.networkLayer", code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                        errorCallback(error)
                    }
                }
            case .failure(let error):
                failureCallback(error)
            }
        }
    }
    
}
