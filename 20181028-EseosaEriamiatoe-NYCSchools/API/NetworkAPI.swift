//
//  NetworkAPI.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import Foundation
import Moya



enum NetworkAPI {
    case schoolList
    case schoolDetails(dbn: String)
}

extension NetworkAPI: TargetType {
    
    var headers: [String : String]? {
        return [:]
    }
    
    var baseURL: URL {
        switch self {
        case .schoolList:
            return URL(string:"https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        case .schoolDetails:
            return URL(string:"https://data.cityofnewyork.us/resource/734v-jeq5.json")!
        }
    }
    
    var path: String {
        switch self {
        default:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
            case  .schoolDetails(let dbn):
            return .requestParameters(parameters:["dbn":dbn] , encoding: URLEncoding.default)
        default:
            return .requestParameters(parameters:[:] , encoding: URLEncoding.default)
        }
    }
}




