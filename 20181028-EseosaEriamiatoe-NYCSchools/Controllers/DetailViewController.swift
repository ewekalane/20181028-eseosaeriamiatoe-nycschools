//
//  DetailViewController.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var schoolDetailTextView: UITextView!
    public var school: School?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = school?.schoolName
        loadSchoolDetails()
    }
    
    func updateDetail(schoolDetail: SchoolDetail) {
        self.schoolDetailTextView.text = "Number of SAT Takers:\n \(schoolDetail.numOfSatTestTakers)" + "\n\n" + "Critical Reading Avg Score:\n \(schoolDetail.satCriticalReadingAvgScore)" + "\n\n" + "Math Avg Score:\n \(schoolDetail.satMathAvgScore)" + "\n\n" + "Writing Avg Score:\n \(schoolDetail.satWritingAvgScore)" + "\n\n"
    }
    

    @objc private func loadSchoolDetails() {
        networkAdapter.request(target: .schoolDetails(dbn: (school?.dbn)!), success: { (response) in
            do{
                let result = try JSONDecoder().decode([SchoolDetail].self, from: response.data)
                DispatchQueue.main.async {
                    if (result.count > 0){
                        self.updateDetail(schoolDetail: result[0])
                    } else{
                        self.schoolDetailTextView.text = "No result available"
                    }
                }
            } catch let err {
                self.schoolDetailTextView.text = "Unable to load detail" + "\n" + "\(err.localizedDescription)"
            }
        }, error: { (error) in
             self.schoolDetailTextView.text = "Unable to load detail" + "\n" + "\(error.localizedDescription)"
        }, failure: { (error) in
             self.schoolDetailTextView.text = "Unable to load detail" + "\n" + "\(error.localizedDescription)"
        })
        
    }

}
