//
//  ViewController.swift
//  20181028-EseosaEriamiatoe-NYCSchools
//
//  Created by Eseosa Eriamiatoe on 10/28/18.
//  Copyright © 2018 Eseosa. All rights reserved.
//

import IGListKit
import UIKit


final class ViewController: UIViewController, ListAdapterDataSource, ListSingleSectionControllerDelegate {
    
    var errorLabel: UILabel!
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    var data:[School] = []
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(loadSchools))
        
        let screenSize = UIScreen.main.bounds
        errorLabel = UILabel(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
        errorLabel.backgroundColor=UIColor.groupTableViewBackground
        errorLabel.textAlignment = .center
        errorLabel.textColor = UIColor.darkGray
        errorLabel.text = "Loading schools..."
        
        view.addSubview(collectionView)
        view.backgroundColor = UIColor.white
        collectionView.backgroundColor = UIColor.white
        adapter.collectionView = collectionView
        adapter.dataSource = self
        title = "NYC Public Schools"
        loadSchools()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
        
    }
    
    // MARK: - ListAdapterDataSource
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return data as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        let configureBlock = { (item: Any, cell: UICollectionViewCell) in
            guard let cell = cell as? ListCell, let school = item as? School else { return }
            cell.text = school.schoolName
        }
        
        let sizeBlock = { (item: Any, context: ListCollectionContext?) -> CGSize in
            guard let context = context else { return CGSize() }
            return CGSize(width: context.containerSize.width, height: 44)
        }
        let sectionController = ListSingleSectionController(nibName: ListCell.nibName,
                                                            bundle: nil,
                                                            configureBlock: configureBlock,
                                                            sizeBlock: sizeBlock)
        sectionController.selectionDelegate = self
        return sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return errorLabel
    }
    
    // MARK: - ListSingleSectionControllerDelegate
    func didSelect(_ sectionController: ListSingleSectionController, with object: Any) {
         performSegue(withIdentifier: "showDetails", sender: object)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let vc = segue.destination as! DetailViewController
            vc.school = sender as? School
        }
    }

    
    @objc private func loadSchools() {
        networkAdapter.request(target: .schoolList, success: { (response) in
            do{
                let result = try JSONDecoder().decode([School].self, from: response.data)
                DispatchQueue.main.async {
                    self.data = result
                    self.adapter.performUpdates(animated: true, completion: nil)
                }
                
            } catch let err {
                DispatchQueue.main.async {
                    self.errorLabel.text = err.localizedDescription
                }
                self.errorLabel.text = err.localizedDescription
            }
        }, error: { (error) in
            DispatchQueue.main.async {
                self.errorLabel.text = error.localizedDescription
            }
    
        }, failure: { (error) in
            DispatchQueue.main.async {
                self.errorLabel.text = error.localizedDescription
            }
            
        })
        
    }
    
}

